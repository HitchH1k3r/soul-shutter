﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class AssignMaterial : ScriptableWizard
{

	[SerializeField]
	public Material material;

	void OnWizardUpdate()
	{
		helpString = "Select Game Objects";
		isValid = (material != null);
	}

	void OnWizardCreate()
	{
		GameObject[] gameObjects = Selection.gameObjects;
		foreach(GameObject gameObject in gameObjects)
		{
			gameObject.GetComponent<Renderer>().material = material;
		}
	}

	[MenuItem("Custom/Assign Material", false, 4)]
	static void DoAssignMaterial()
	{
		ScriptableWizard.DisplayWizard("Assign Material", typeof(AssignMaterial), "Assign");
	}

}
