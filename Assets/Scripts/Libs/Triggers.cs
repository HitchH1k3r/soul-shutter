﻿using UnityEngine;

namespace Game
{

	public class TriggerLib
	{
		public static void ProcessTrigger(TriggerType trigger, GameObject target)
		{
			if(trigger == TriggerType.LOAD_OBJECT)
			{
				target.SetActive(true);
            }
			else if(trigger == TriggerType.UNLOAD_OBJECT)
			{
				target.SetActive(false);
            }
            else
            {
                target.SendMessage("DoTrigger", trigger);
			}
		}
	}

	public enum TriggerType
	{
		NONE, ACTIVATE_PATHING, TOGGLE_STATE, ENABLE_STATE, DISABLE_STATE, LOAD_OBJECT, UNLOAD_OBJECT
	}

}