﻿using UnityEngine;

namespace Game
{

	public class DataBank
	{
		public static bool GamePaused = false;
		public static GameObject RailCart;
		public static float attackFOV;
		public static int photoScoreAdder;
		public static bool needFlash = true;

		public static RenderTexture[] finalPhotoImages;
		public static int[] finalPhotoScores;
		public static bool[] finalPhotoInventory;
		public static int finalScore;
	}

}