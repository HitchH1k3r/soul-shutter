﻿using System.Collections.Generic;
using UnityEngine;

namespace Game
{
	
	public class Math
	{
		
		public static Vector3 QurpInVecs(Vector3 from, Vector3 to, float time)
		{
			Vector3 dir = to - from;
			dir *= time * time;
			dir += from;
			return dir;
		}

		public static Vector3 QurpOutVecs(Vector3 from, Vector3 to, float time)
		{
			Vector3 dir = to - from;
			dir *= -time * (time - 2);
			dir += from;
			return dir;
		}

		public static Vector3 QurpVecs(Vector3 from, Vector3 to, float time)
		{
			Vector3 dir = to - from;
			float factor = 0;
			time *= 2;
			if(time < 1)
			{
				factor = 0.5f * time * time;
			}
			else
			{
				--time;
				factor = -0.5f * (time * (time - 2) - 1);
			}
			dir *= factor;
			dir += from;
			return dir;
		}

	}

	public class RaySorter : IComparer<RaycastHit>
	{

		public int Compare(RaycastHit a, RaycastHit b)
        {
			if(a.distance == b.distance)
			{
				return 0;
            }
			else if(a.distance < b.distance)
			{
				return -1;
            }
			else
			{
				return 1;
            }
        }

	}
    
}