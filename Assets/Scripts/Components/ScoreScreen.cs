﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class ScoreScreen : MonoBehaviour
{

	public Texture2D white;
	public AudioClip doorClose;

	private int imageIndex = 0;
	private AudioSource sfx;

	void Start()
	{
		Cursor.lockState = CursorLockMode.None;
		Cursor.visible = true;
		sfx = this.gameObject.GetComponent<AudioSource>();
		sfx.PlayOneShot(doorClose);
	}

	void Update()
	{
		if(Input.GetMouseButtonDown(0))
		{
			if(Input.mousePosition.y < 50 && Input.mousePosition.x > (Screen.width / 2) - 200 && Input.mousePosition.x < (Screen.width / 2) + 200)
			{
				Application.LoadLevel("Title");
			}
			else if(Input.mousePosition.x < Screen.width / 2)
			{
				if(imageIndex > 0)
				{
					--imageIndex;
				}
			}
			else
			{
				if(imageIndex < Game.DataBank.finalPhotoImages.Length - 1 && Game.DataBank.finalPhotoImages[imageIndex + 1] != null)
				{
					++imageIndex;
				}
			}
		}
	}

	void OnGUI()
	{
		GUI.color = Color.white;

		int width = 720;
		int height = 480;

		if(Screen.height - 80 < 720)
		{
			height = (int) ((Screen.height - 80) * 0.6666666666666f);
			width = (int) (1.5f * height);
		}

		if (Game.DataBank.finalPhotoImages != null)
		{
			if (imageIndex > 0)
			{
				DrawImage (width, height, -width - 20, 0, imageIndex - 1);
			}
			DrawImage (width, height, 0, 0, imageIndex);
			if (imageIndex < Game.DataBank.finalPhotoImages.Length - 1 && Game.DataBank.finalPhotoImages[imageIndex + 1] != null)
			{
				DrawImage(width, height, width + 20, 0, imageIndex + 1);
			}
		}

		GUIStyle style = new GUIStyle();
		style.alignment = TextAnchor.LowerRight;
		style.normal.textColor = Color.white;
		style.fontSize = 40;
		
		GUI.Label(new Rect(Screen.width - 610, 7, 600, 40), "★" + Game.DataBank.finalScore, style);

		if(Input.mousePosition.y < 50 && Input.mousePosition.x > (Screen.width / 2) - 200 && Input.mousePosition.x < (Screen.width / 2) + 200)
		{
			style.normal.textColor = Color.white;
		}
		else
		{
			style.normal.textColor = Color.grey;
		}
		style.alignment = TextAnchor.LowerCenter;
		GUI.Label(new Rect((Screen.width - 400) / 2, Screen.height - 50, 400, 40), "RESTART GAME", style);

	}

	void DrawImage(int width, int height, int offsetX, int offsetY, int photo)
	{
		GUI.DrawTexture(new Rect((Screen.width - width - 10) / 2 + offsetX, (Screen.height - height - 50) / 2 + offsetY, width + 10, height + 50), white);
		GUI.DrawTexture(new Rect((Screen.width - width) / 2 + offsetX, (Screen.height - height - 40) / 2 + offsetY, width, height), Game.DataBank.finalPhotoImages[photo]);
		
		if(!Game.DataBank.finalPhotoInventory[photo])
		{
			GUI.color = Color.red;
			
			GUIUtility.RotateAroundPivot(45, new Vector2(Screen.width / 2 + offsetX, Screen.height / 2 + offsetY));
			GUI.DrawTexture(new Rect((Screen.width - width / 5) / 2 + offsetX, (Screen.height - width * 1.1f) / 2 + offsetY, width / 5, width * 1.1f), white);
			GUIUtility.RotateAroundPivot(90, new Vector2(Screen.width / 2 + offsetX, Screen.height / 2 + offsetY));
			GUI.DrawTexture(new Rect((Screen.width - width / 5) / 2 + offsetX, (Screen.height - width * 1.1f) / 2 + offsetY, width / 5, width * 1.1f), white);
			GUIUtility.RotateAroundPivot(180 + 45, new Vector2(Screen.width / 2 + offsetX, Screen.height / 2 + offsetY));

			GUI.color = Color.white;
		}

		GUIStyle style = new GUIStyle();
		style.fontSize = 40;
		style.alignment = TextAnchor.LowerCenter;
		style.normal.textColor = Color.black;
		GUI.Label (new Rect ((Screen.width - 600) / 2 + offsetX, ((Screen.height - height - 40) / 2) + height + 7 + offsetY, 600, 40), "★" + Game.DataBank.finalPhotoScores[photo], style);
	}

}
