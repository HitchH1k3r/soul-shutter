﻿using UnityEngine;
using System.Collections;

public class RailController : MonoBehaviour
{

	public RailWaypoint start;

	private float transitionTime = 0.0f;
	private float waitTimeLeft = 0.0f;
	private RailWaypoint nextWaypoint;
	private bool fleeing = false;
	private Vector3 fleeStart;
	private Quaternion fleeStartAngle;
	private Vector3 fleeEnd;
	private Quaternion fleeEndAngle;

	void OnEnable()
	{
		Game.DataBank.RailCart = this.gameObject;
		this.transform.position = start.transform.position;
		nextWaypoint = start.nextWaypoint;
	}
	
	void Update()
	{
		if(!Game.DataBank.GamePaused)
		{
			if(fleeing)
			{
				transitionTime += Time.deltaTime * 0.5f;
				this.transform.position = Vector3.Lerp(fleeStart, fleeEnd, Mathf.Clamp01(transitionTime * 0.5f));
				this.transform.rotation = Quaternion.Slerp(fleeStartAngle, fleeEndAngle, Mathf.Clamp01(transitionTime * 8.0f));
			}
			else
			{
				if(waitTimeLeft > 0.0f)
				{
					waitTimeLeft -= Time.deltaTime;
					if(waitTimeLeft <= 0.0f)
					{
						waitTimeLeft = 0.0f;
					}
				}
				else
				{
					transitionTime += Time.deltaTime;
					if(transitionTime > start.transitionTime)
					{
						transitionTime = 0.0f;
						start = nextWaypoint;
						if(start.IsForkActive())
						{
							nextWaypoint = start.forkWaypoint;
	                    }
						else
						{
							nextWaypoint = start.nextWaypoint;
						}
						if(start.trigger != Game.TriggerType.NONE)
						{
							foreach(GameObject triggerTarget in start.triggerTargets)
							{
								Game.TriggerLib.ProcessTrigger(start.trigger, triggerTarget);
							}
						}
						waitTimeLeft = start.waitTime;
					}
					else
					{
						float t = (transitionTime / start.transitionTime);
						if(start.waitTime > 0)
						{
							if(nextWaypoint.waitTime > 0)
							{
								this.transform.position = Game.Math.QurpVecs(start.transform.position, nextWaypoint.transform.position, t);
							}
							else
							{
								this.transform.position = Game.Math.QurpInVecs(start.transform.position, nextWaypoint.transform.position, t);
							}
						}
						else
						{
							if(nextWaypoint.waitTime > 0)
							{
								this.transform.position = Game.Math.QurpOutVecs(start.transform.position, nextWaypoint.transform.position, t);
							}
							else
							{
								this.transform.position = Vector3.Lerp(start.transform.position, nextWaypoint.transform.position, t);
							}
						}

						this.transform.rotation = Quaternion.Slerp(start.transform.rotation, nextWaypoint.transform.rotation, t);
					}
				}
			}
		}
	}

	void TriggerBacktrack()
	{
		fleeing = true;
		transitionTime = 0.0f;
		fleeStart = transform.position;
		fleeStartAngle = transform.rotation;
		Vector3 angles = start.transform.rotation.eulerAngles;
		fleeEndAngle = Quaternion.Euler(angles.x, angles.y + 180, angles.z);
		fleeEnd = transform.position + ((fleeEndAngle * Vector3.forward) * 50);
	}

}
