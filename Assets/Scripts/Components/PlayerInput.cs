﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class PlayerInput : MonoBehaviour
{

	public Texture2D reticle_center, reticle_corner, white, film_icon;
	public AudioClip shutter, charge, damage;
	public Transform camera_transform;
	public Transform flashlight_transform;
	public GameObject flashlight_toggler;
	public Light cameraflash_toggler;
	public Camera DEBUG_CAMERA;

	private Vector2 lookOffset = Vector2.zero;
	private float zoom = 0.5f;
	private float flashCooldown = 0.0f;
	private int flashStage = 0;
	private AudioSource sfx;
	private float hitCooldown = 0;
	private int totalScore = 0;
	private TitleScreen menu;
	private float pauseCooldown = 0;

	private const int NUM_PHOTOS = 42;

	// 720 x 480
	private RenderTexture[] photoImages = new RenderTexture[NUM_PHOTOS];
	private int[] photoScores = new int[NUM_PHOTOS];
	private bool[] photoInventory = new bool[NUM_PHOTOS];
	private int photoIndex = 0;
	private bool fleeing = false;
	private float fleeTime = 0;

	void Start()
	{
		sfx = this.GetComponent<AudioSource>();
		menu = this.GetComponent<TitleScreen>();
		pauseCooldown = 0.5f;
	}

	void Update()
	{
		if(!Game.DataBank.GamePaused)
		{
			if(Input.GetButtonDown("Cancel"))
			{
				menu.enabled = true;
				pauseCooldown = 0.1f;
			}

			if(fleeing)
			{
				fleeTime += Time.deltaTime * 0.5f;
				if(fleeTime > 1)
				{
					Application.LoadLevel("HighScore");
				}
			}

			if(pauseCooldown > 0.0f)
			{
				pauseCooldown -= Time.deltaTime;
			}

			if(flashCooldown > 0.0f)
			{
				flashCooldown -= Time.deltaTime;
				if(flashStage == 0 && flashCooldown > 2.75f)
				{
					cameraflash_toggler.intensity = 5.0f - ((3 - flashCooldown) * 20);
				}
				else if(flashStage == 0)
				{
					flashlight_toggler.SetActive(true);
					cameraflash_toggler.enabled = false;
					if(Game.DataBank.needFlash)
					{
						flashStage = 1;
					}
					else
					{
						flashCooldown = 0;
					}
					if(photoIndex >= NUM_PHOTOS)
					{
						LoseTheGame();
					}
				}
				else if(flashStage == 1 && flashCooldown < 2)
				{
					sfx.PlayOneShot(charge, 0.25f);
					flashStage = 2;
				}
			}

			if(hitCooldown > 0)
			{
				hitCooldown -= Time.deltaTime;
			}

			if(Cursor.lockState == CursorLockMode.None)
			{
				Cursor.lockState = CursorLockMode.Locked;
				Cursor.visible = false;
			}

			zoom += Input.mouseScrollDelta.y * -0.05f;

			if(Input.GetMouseButton(1))
			{
				zoom += Input.GetAxisRaw("Mouse Y") * -0.1f;
			}
			else
			{
				lookOffset.x = Mathf.Clamp01(lookOffset.x + Input.GetAxisRaw("Mouse X") * 0.005f);
				lookOffset.y = Mathf.Clamp01(lookOffset.y - Input.GetAxisRaw("Mouse Y") * 0.01f);
			}

			zoom = Mathf.Clamp01(zoom);

			camera_transform.localEulerAngles = new Vector3((lookOffset.y * 2 - 1) * 30, (lookOffset.x * 2 - 1) * 70, 0.0f);
			flashlight_transform.localRotation = Quaternion.Lerp(flashlight_transform.localRotation, Quaternion.Euler(new Vector3((lookOffset.y * 2 - 1) * 30, (lookOffset.x * 2 - 1) * 100, 0.0f)), Time.deltaTime * 10.0f);

			if(Input.GetButtonDown("Fire1") && flashCooldown <= 0 && photoIndex < NUM_PHOTOS && !fleeing && pauseCooldown <= 0)
			{
				sfx.PlayOneShot(shutter);
				flashCooldown = 3.0f;
				flashStage = 0;
				if(Game.DataBank.needFlash)
				{
					flashlight_toggler.SetActive(false);
					cameraflash_toggler.enabled = true;
					cameraflash_toggler.intensity = 2.0f;
				}
				TakePhoto();
				if(Game.DataBank.needFlash)
				{
					cameraflash_toggler.intensity = 5.0f;
				}
            }
        }
    }
    
    void OnGUI()
	{
		float sizer = (zoom + 0.5f) * Screen.height / 10.0f;
		Vector2 position = new Vector2(Screen.width / 2, Screen.height / 2);
		Vector2 offset = new Vector2((lookOffset.x * 2 - 1) * (Screen.width * 0.5f - sizer * 3), (lookOffset.y * 2 - 1) * (Screen.height * 0.5f - sizer * 2));

		GUI.DrawTexture(new Rect(position.x + offset.x - 8, position.y + offset.y - 8, 16, 16), reticle_center);
		GUI.DrawTexture(new Rect(position.x + offset.x - (sizer * 3), position.y + offset.y - (sizer * 2), 16, 16), reticle_corner);
		GUIUtility.RotateAroundPivot (90, position + offset);
		GUI.DrawTexture(new Rect(position.x + offset.x - (sizer * 2), position.y + offset.y - (sizer * 3), 16, 16), reticle_corner);
		GUIUtility.RotateAroundPivot (90, position + offset);
		GUI.DrawTexture(new Rect(position.x + offset.x - (sizer * 3), position.y + offset.y - (sizer * 2), 16, 16), reticle_corner);
		GUIUtility.RotateAroundPivot (90, position + offset);
		GUI.DrawTexture(new Rect(position.x + offset.x - (sizer * 2), position.y + offset.y - (sizer * 3), 16, 16), reticle_corner);
		GUIUtility.RotateAroundPivot (90, position + offset);

		if(flashCooldown > 2.5f && Game.DataBank.needFlash)
		{
			GUI.color = new Color(1.0f, 1.0f, 1.0f, flashCooldown - 2.25f);
			GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), white);
		}

		GUI.color = new Color(0.0f, 0.0f, 0.0f, 0.5f);
		float h = Screen.height * 0.15f;
		GUI.DrawTexture(new Rect(0, Screen.height - h - 30, Screen.width, h + 30), white);

		GUI.color = Color.white;
		float w = (h - 16) * 1.5f;
		int slideIndex = 0;
		for(int i = photoIndex - 1; i >= 0 && slideIndex * (w + 8) + 4 < Screen.width; --i)
		{
			if(photoImages[i] != null && photoInventory[i])
			{
				GUI.DrawTexture(new Rect(slideIndex * (w + 8) + 2, Screen.height - h + 2, w + 4, h - 4), white);
				GUI.DrawTexture(new Rect(slideIndex * (w + 8) + 4, Screen.height - h + 4, w, h - 16), photoImages[i]);
				++slideIndex;
			}
        }

		Vector2 scoreOffset = Vector2.zero;
		Vector2 filmOffset = Vector2.zero;
		float scoreScale = 1;
		float filmScale = 1;

		if(fleeing)
		{
			GUI.color = new Color(0.0f, 0.0f, 0.0f, fleeTime * fleeTime);
			GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), white);

			scoreOffset.y = (Screen.height - h - 32) * -fleeTime;
			if(photoIndex >= NUM_PHOTOS)
			{
				filmOffset.x = (Screen.width - 100) / 2 * fleeTime;
				filmOffset.y = (Screen.height - h) / 2 * -fleeTime;
				filmScale = (4 * fleeTime) + 1;
			}
			else
			{
				filmOffset.x = -100 * fleeTime;
			}
			scoreScale = fleeTime + 1;

			GUI.color = Color.white;
		}

		GUIStyle style = new GUIStyle();
		style.alignment = TextAnchor.LowerRight;
		style.normal.textColor = Color.white;
		style.fontSize = (int) (20 * scoreScale);

		GUI.Label(new Rect(Screen.width - 610 + scoreOffset.x, Screen.height - h - 25 + scoreOffset.y, 600, 20 * scoreScale), "★" + totalScore, style);

		if(fleeing && photoIndex >= NUM_PHOTOS && fleeTime > 0.9f)
		{
			GUI.color = new Color(1.0f, 1.0f, 1.0f, 1.0f - ((fleeTime - 0.9f) * 10.0f));
		}

		style.alignment = TextAnchor.LowerLeft;
		if(photoIndex > NUM_PHOTOS - 10)
		{
			style.normal.textColor = Color.red;
		}
		style.fontSize = (int) (20 * filmScale);
		GUI.DrawTexture(new Rect(5 + filmOffset.x, Screen.height - h - 25 - 3 * filmScale + filmOffset.y, 20 * filmScale, 20 * filmScale), film_icon);
		GUI.Label(new Rect(10 + 20 * filmScale + filmOffset.x, Screen.height - h - 25 + filmOffset.y, 100 * filmScale, 20 * filmScale), "" + (NUM_PHOTOS - photoIndex), style);

		if(hitCooldown > 0)
		{
			GUI.color = new Color(1.0f, 0.0f, 0.0f, hitCooldown);
			GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), white);
		}
	}
    
	void PlayerHit(GhostController ghost)
	{
		if(hitCooldown <= 0 && !fleeing)
		{
			int searchIndex = photoIndex - 1;
			while(searchIndex >= 0)
			{
				if(photoInventory[searchIndex])
				{
					photoInventory[searchIndex] = false;
					totalScore -= photoScores[searchIndex];
					break;
				}
				--searchIndex;
			}
			sfx.PlayOneShot(damage); 
			hitCooldown = 1.0f;
			if(searchIndex < 0)
			{
				LoseTheGame();
			}
		}
	}

    private void TakePhoto()
	{
		float sizer = (zoom + 0.5f) * Screen.height / 10.0f;
        Vector2 offset = new Vector2((lookOffset.x * 2 - 1) * (Screen.width * 0.5f - sizer * 3), (lookOffset.y * -2 + 1) * (Screen.height * 0.5f - sizer * 2));
		offset += new Vector2(Screen.width / 2, Screen.height / 2);
		Ray subScreenRay = Camera.main.ScreenPointToRay(offset);
		RenderTexture tempTex = Camera.main.targetTexture;
		float tempAspect = Camera.main.aspect;
		float tempFOV = Camera.main.fieldOfView;
		Quaternion tempRotation = Camera.main.transform.rotation;
		Color tempColor = Camera.main.backgroundColor;
		photoImages[photoIndex] = new RenderTexture(720, 480, (int) RenderTextureFormat.Default);
		Camera.main.aspect = 1.5f;
		Camera.main.targetTexture = photoImages[photoIndex];
		Camera.main.fieldOfView *= (zoom + 0.5f) / 2.5f;
		Camera.main.transform.forward = subScreenRay.direction;
        Camera.main.backgroundColor = Color.black;
		Game.DataBank.attackFOV = Camera.main.fieldOfView;
		Game.DataBank.photoScoreAdder = 0;
		PhotogenicBehaviour.isVulnerable = true;
		Camera.main.Render();
		PhotogenicBehaviour.isVulnerable = false;
		photoInventory[photoIndex] = true;
		photoScores[photoIndex++] = Game.DataBank.photoScoreAdder;
		totalScore += Game.DataBank.photoScoreAdder;
		Camera.main.targetTexture = tempTex;
		Camera.main.aspect = tempAspect;
		Camera.main.fieldOfView = tempFOV;
		Camera.main.transform.rotation = tempRotation;
		Camera.main.backgroundColor = tempColor;
	}
    
	private void LoseTheGame()
	{
		fleeing = true;
		SendMessageUpwards("TriggerBacktrack");
		Game.DataBank.finalPhotoImages = photoImages;
		Game.DataBank.finalPhotoScores = photoScores;
		Game.DataBank.finalPhotoInventory = photoInventory;
		Game.DataBank.finalScore = totalScore;
	}

}
