﻿using UnityEngine;
using System.Collections;

public class BasicAnimator : MonoBehaviour
{

	public Vector3 rotationSpeed = Vector3.zero;
	public float bobTime = 0;
	public Vector3 bobOffset = Vector3.zero;

	private Vector3 basePosition;
	private float bobTimer = 0;

	void Start()
	{
		basePosition = transform.position;
	}

	void Update()
	{
		this.transform.rotation *= Quaternion.Euler(rotationSpeed * Time.deltaTime);
		if(bobTime > 0)
		{
			bobTimer += Time.deltaTime / (bobTime * 2);
			while(bobTimer > 2)
			{
				bobTimer -= 2;
			}
			if(bobTimer > 1)
			{
				transform.position = Game.Math.QurpVecs(basePosition, basePosition + bobOffset, bobTimer - 1);
			}
			else
			{
				transform.position = Game.Math.QurpVecs(basePosition + bobOffset, basePosition, bobTimer);
			}
		}
	}

}
