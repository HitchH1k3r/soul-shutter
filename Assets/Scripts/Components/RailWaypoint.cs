﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;

public class RailWaypoint : MonoBehaviour
{

	public RailWaypoint nextWaypoint;
	public float waitTime = 0;
	public float transitionTime = 1;
	public RailWaypoint forkWaypoint;
	public Game.TriggerType trigger = Game.TriggerType.NONE;
	public GameObject[] triggerTargets;

	private bool forkActive = false;

#if UNITY_EDITOR
	private static Camera EDIT_cam;
	private static GameObject EDIT_activeGameObject;

	void OnDrawGizmos()
	{
		// Draw frustrum:
		if (UnityEditor.Selection.Contains(this.gameObject))
		{
			Gizmos.color = Color.white;
		}
		else
		{
			Gizmos.color = Color.grey;
		}
		Gizmos.matrix *= Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);
		Gizmos.DrawFrustum (Vector3.zero, 60.0f, 1.0f, 0.0f, 1.33f);

		// Draw clear sphere to select easier:
		Gizmos.color = Color.clear;
		Gizmos.DrawSphere(new Vector3(0.0f, 0.0f, 0.6f), 0.75f);

		// Draw rail path:
		Gizmos.matrix = Matrix4x4.identity;
		if (nextWaypoint != null)
		{
			Gizmos.color = Color.blue;
			Gizmos.DrawLine (this.transform.position, nextWaypoint.transform.position);
        }
		if (forkWaypoint != null)
		{
			if(forkActive)
			{
				Gizmos.color = Color.yellow;
            }
			else
			{
				Gizmos.color = Color.grey;
			}
            Gizmos.DrawLine (this.transform.position, forkWaypoint.transform.position);
        }
        
        // Add a camera for preview (if in editor):
		if(EDIT_activeGameObject != Selection.activeGameObject)
		{
			EDIT_activeGameObject = Selection.activeGameObject;
			if(EDIT_cam != null)
			{
				DestroyImmediate(EDIT_cam);
			}
			if(EDIT_activeGameObject != null)
			{
				RailWaypoint w = EDIT_activeGameObject.GetComponent<RailWaypoint>();
				if(w != null)
				{
					w.gameObject.AddComponent(typeof(Camera));
					EDIT_cam = w.gameObject.GetComponent<Camera>();
				}
			}
		}
	}
#endif

	public bool IsForkActive()
	{
		return forkActive;
	}

	void DoTrigger(Game.TriggerType triggerType)
	{
		if(triggerType == Game.TriggerType.TOGGLE_STATE)
		{
			forkActive = !forkActive;
        }
		else if(triggerType == Game.TriggerType.ENABLE_STATE)
		{
			forkActive = true;
        }
		else if(triggerType == Game.TriggerType.DISABLE_STATE)
		{
			forkActive = false;
        }
    }
    
}
