﻿using UnityEngine;
using System.Collections;

public class MobIO : MobWaypoint {

	public MobIOType waypointType;
	public GameObject spawnPrefab;
	public float spawnDelay = 0;
	public float spawnInterval = 5;
	public bool isActive = true;
    
    private float spawnerCooldown;

	void Start()
	{
		spawnerCooldown = spawnInterval;
	}
	
	void Update()
	{
		if(!Game.DataBank.GamePaused && isActive)
		{
			if(waypointType == MobIOType.SPAWNER)
			{
				if(spawnDelay > 0)
				{
					spawnDelay -= Time.deltaTime;
				}
				else
				{
	                spawnerCooldown -= Time.deltaTime;
					if(spawnerCooldown < 0)
					{
						spawnerCooldown = spawnInterval;
						GameObject go = (GameObject) Instantiate(spawnPrefab, this.transform.position, this.transform.rotation);
						GhostController controller = go.GetComponent<GhostController>();
						controller.JoinPath(this);
					}
				}
			}
		}
	}

	void OnDrawGizmos()
	{
		// Draw sphere:
		if(waypointType == MobIOType.SPAWNER)
		{
			Gizmos.color = Color.green;
		}
		else
		{
			Gizmos.color = Color.red;
		}
		Gizmos.DrawWireSphere(this.transform.position, 0.25f);

		// Draw clear sphere to select easier:
		Gizmos.color = Color.clear;
		Gizmos.DrawSphere(this.transform.position, 0.25f);

		// Draw paths:
		Gizmos.color = Color.green;
		if(nextWaypoints != null)
		{
			foreach(MobWaypoint waypoint in nextWaypoints)
			{
				// (int) Random.Range(0, nextWaypoints.Length)
				if (waypoint != null)
				{
					Gizmos.DrawLine(this.transform.position, waypoint.transform.position);
				}
			}
		}
	}

	public enum MobIOType
	{
		SPAWNER, DESTROYER
	}

	void DoTrigger(Game.TriggerType triggerType)
	{
		if(triggerType == Game.TriggerType.TOGGLE_STATE)
		{
			isActive = !isActive;
        }
		else if(triggerType == Game.TriggerType.ENABLE_STATE)
		{
			isActive = true;
        }
		else if(triggerType == Game.TriggerType.DISABLE_STATE)
		{
			isActive = false;
        }
    }
    
}
