﻿using UnityEngine;
using System.Collections;

public class MobWaypoint : MonoBehaviour
{

	public MobWaypoint[] nextWaypoints;
	public MobBehavior[] nextBehaviors;

#if UNITY_EDITOR
	void OnDrawGizmos()
	{
		// Draw sphere:
		if (UnityEditor.Selection.Contains(this.gameObject))
		{
			Gizmos.color = Color.white;
		}
		else
		{
			Gizmos.color = Color.grey;
		}
		Gizmos.DrawWireSphere(this.transform.position, 0.25f);
		
		// Draw clear sphere to select easier:
		Gizmos.color = Color.clear;
		Gizmos.DrawSphere (this.transform.position, 0.25f);
		
		// Draw paths:
		Gizmos.color = Color.green;
		if(nextWaypoints != null)
		{
			foreach(MobWaypoint waypoint in nextWaypoints)
			{
				if (waypoint != null)
				{
					Gizmos.DrawLine(this.transform.position, waypoint.transform.position);
				}
			}
		}
	}
#endif

	public enum MobBehavior
	{
		ADVANCE_PATH, BREAK_ATTACK, HAUNT_PLAYER, END_PATHING
	}

}
