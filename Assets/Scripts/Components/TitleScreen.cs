﻿using UnityEngine;
using System.Collections;

public class TitleScreen : MonoBehaviour
{

	public bool inGame = false;
	public Texture2D white;

	void OnEnable()
	{
		Game.DataBank.GamePaused = true;
	}
	
	void Update()
	{
		Cursor.lockState = CursorLockMode.None;
		Cursor.visible = true;
		if(Input.GetMouseButtonDown(0))
		{
			int button = GetButton();
			if(button == 1)
			{
				if(inGame)
				{
					this.enabled = false;
				}
				else
				{
					Application.LoadLevel("Game");
				}
				Cursor.lockState = CursorLockMode.Locked;
				Cursor.visible = false;
				Game.DataBank.GamePaused = false;
			}
			else if(button == 2)
			{
			#if UNITY_WEBPLAYER
				if(Screen.fullScreen)
				{
					Screen.SetResolution(900, 600, false);
				}
				else
				{
					Screen.SetResolution(Screen.currentResolution.width, Screen.currentResolution.height, true);
                }
			#else
				Screen.fullScreen = !Screen.fullScreen;
			#endif
			}
			else if(button == 3)
			{
				Application.Quit();
			}
		}
	}

	void OnGUI()
	{
		if(inGame)
		{
			GUI.color = new Color(0.0f, 0.0f, 0.0f, 0.5f);
			GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), white);
		}

		GUI.color = Color.white;
		GUIStyle style = new GUIStyle();
		style.fontSize = 20;
		style.alignment = TextAnchor.LowerCenter;

		int button = GetButton();

		style.normal.textColor = (button == 1 ? Color.white : Color.grey);
		GUI.Label(new Rect((Screen.width - 200) / 2, (Screen.height - 20) / 2 - 50, 200, 20), (inGame ? "RESUME GAME" : "START GAME"), style);

		style.normal.textColor = (button == 2 ? Color.white : Color.grey);
		GUI.Label(new Rect((Screen.width - 200) / 2, (Screen.height - 20) / 2     , 200, 20), "FULL SCREEN", style);

	#if UNITY_STANDALONE
		style.normal.textColor = (button == 3 ? Color.white : Color.grey);
		GUI.Label(new Rect((Screen.width - 200) / 2, (Screen.height - 20) / 2 + 50, 200, 20), "EXIT GAME", style);
	#endif

		if(!inGame)
		{
			style.normal.textColor = Color.white;
			GUI.Label(new Rect(0, Screen.height - 70, Screen.width, 20), "MOUSE :: LOOK", style);
			GUI.Label(new Rect(0, Screen.height - 50, Screen.width, 20), "WHEEL/RIGHT MOUSE :: ZOOM", style);
			GUI.Label(new Rect(0, Screen.height - 30, Screen.width, 20), "LEFT MOUSE :: PHOTO", style);
        }
    }
    
	private int GetButton()
	{
		if(Input.mousePosition.x > (Screen.width - 200) / 2 && Input.mousePosition.x < (Screen.width + 200) / 2)
		{
			if(Input.mousePosition.y > (Screen.height - 20) / 2 + 50 && Input.mousePosition.y < (Screen.height + 20) / 2 + 50)
			{
				return 1;
			}
			else if(Input.mousePosition.y > (Screen.height - 20) / 2 && Input.mousePosition.y < (Screen.height + 20) / 2)
			{
				return 2;
			}
		#if UNITY_STANDALONE
			else if(Input.mousePosition.y > (Screen.height - 20) / 2 - 50 && Input.mousePosition.y < (Screen.height + 20) / 2 - 50)
			{
				return 3;
			}
		#endif
		}
		return 0;
	}

}
