﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class GhostController : MonoBehaviour
{

	public MobWaypoint currentWaypoint;
	public float moveSpeed = 0.5f;
	public float attackSpeed = 1.75f;
	public AudioClip attackSound, activateSound;
	public bool noPathing = false;
	public float HP = 10;
    
	public Game.TriggerType deathTrigger;
	public GameObject deathTriggerTarget;

    private MobWaypoint nextWaypoint;
	private Transform targetPoint;
	private Vector3 vel;
	private Quaternion targetRotation;
	private float bobing = 0;
	private bool attacking = false;
	private AudioSource sfx;

	void Start()
	{
		sfx = this.GetComponent<AudioSource>();
	}

	void Update()
	{
		if(!Game.DataBank.GamePaused)
		{
			if(bobing > 2)
			{
				transform.position -= new Vector3(0, (bobing - 3) * 0.5f, 0);
			}
			else
			{
				transform.position += new Vector3(0, (bobing - 1) * 0.5f, 0);
			}
			bobing += Time.deltaTime * 4.0f;
			if(bobing > 4)
			{
				bobing = 0;
			}

			vel *= 0.8f;
			if(!noPathing)
			{
				if(targetPoint != null)
				{
					Vector3 dir = targetPoint.position - this.transform.position;
					float mag = dir.magnitude;
					if(mag < 0.1f)
					{
						targetPoint = null;
					}
					dir.Normalize();
					Vector3 d = dir;
					d.Scale(new Vector3(1, 0, 1));
					transform.localRotation = Quaternion.Lerp(transform.localRotation, Quaternion.LookRotation(d), Time.deltaTime * 2.5f);
					dir *= (attacking ? attackSpeed : moveSpeed) * Time.deltaTime;
					vel += dir;
				}
				else if(currentWaypoint != null)
				{
					currentWaypoint = nextWaypoint;
					DoBehavior();
				}
			}

			transform.position += vel;
			if(bobing > 2)
			{
				transform.position += new Vector3(0, (bobing - 3) * 0.5f, 0);
			}
			else
			{
				transform.position -= new Vector3(0, (bobing - 1) * 0.5f, 0);
			}
		}
	}

	public void JoinPath(MobWaypoint startPoint)
	{
		currentWaypoint = startPoint;
		DoBehavior();
	}

	private void DoBehavior()
	{
		if(attacking)
		{
			Game.DataBank.RailCart.BroadcastMessage("PlayerHit", this);
			if(deathTrigger != Game.TriggerType.NONE)
			{
				Game.TriggerLib.ProcessTrigger(deathTrigger, deathTriggerTarget);
			}
            Destroy(this.gameObject);
            currentWaypoint = null;
		}
		else if(currentWaypoint.GetType() == typeof(MobIO) && ((MobIO) currentWaypoint).waypointType == MobIO.MobIOType.DESTROYER)
		{
			Destroy(this.gameObject);
			currentWaypoint = null;
		}
		else if(currentWaypoint != null)
		{
			MobWaypoint.MobBehavior behavior;
			if(currentWaypoint.nextBehaviors != null && currentWaypoint.nextBehaviors.Length > 0)
			{
				behavior = currentWaypoint.nextBehaviors[(int) Random.Range(0, currentWaypoint.nextBehaviors.Length)];
			}
			else
			{
				behavior = MobWaypoint.MobBehavior.ADVANCE_PATH;
			}
			if(behavior == MobWaypoint.MobBehavior.ADVANCE_PATH)
			{
				nextWaypoint = currentWaypoint.nextWaypoints[(int) Random.Range(0, currentWaypoint.nextWaypoints.Length)];
                targetPoint = nextWaypoint.transform;
			}
			else if (behavior == MobWaypoint.MobBehavior.BREAK_ATTACK)
			{
				nextWaypoint = null;
				targetPoint = Camera.main.transform;
				attacking = true;
				if(attackSound != null)
				{
					sfx.PlayOneShot(attackSound);
				}
			}
		}
		noPathing = (currentWaypoint == null);
	}

	void HitWithCamera(float quality)
	{
		HP -= 20 * quality;
		
		if(HP <= 0)
		{
			if(deathTrigger != Game.TriggerType.NONE)
            {
                Game.TriggerLib.ProcessTrigger(deathTrigger, deathTriggerTarget);
            }
            Destroy(this.gameObject);
		}

		Game.DataBank.photoScoreAdder += (int) (100 * quality);
    }
	
	void DoTrigger(Game.TriggerType triggerType)
	{
		if(triggerType == Game.TriggerType.ACTIVATE_PATHING)
		{
			noPathing = false;
			DoBehavior();
			if(activateSound != null)
			{
				sfx.PlayOneShot(activateSound);
			}
		}
	}

}
