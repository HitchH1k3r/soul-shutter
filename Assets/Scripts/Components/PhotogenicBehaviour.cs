﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider))]
public class PhotogenicBehaviour : MonoBehaviour
{

	public int bonusPoints = 0;
	public bool directionSensitive = true;

	public static bool isVulnerable = false;

	private Collider colliderHelper;
	private float height;

	void Start()
	{
		colliderHelper = this.GetComponent<Collider>();
		height = colliderHelper.bounds.max.y - colliderHelper.bounds.min.y;
	}

	void OnWillRenderObject()
	{
		if(isVulnerable)
		{
			RaycastHit[] rays = Physics.RaycastAll(Camera.main.transform.position, transform.position - Camera.main.transform.position);
			System.Array.Sort(rays, new Game.RaySorter());
			foreach(RaycastHit rayHit in rays)
			{
				PhotogenicBehaviour photoSubject = rayHit.collider.gameObject.GetComponent<PhotogenicBehaviour>();
				if(photoSubject == null)
				{
					break;
                }
				else if(photoSubject == this)
                {
					float distToGhost = (Camera.main.transform.position - transform.position).magnitude;
					float fovNearness = 2 * Mathf.Rad2Deg * Mathf.Atan(height / (distToGhost * 2.0f)) / Game.DataBank.attackFOV;
					if(fovNearness > 1)
					{
						fovNearness = 1.25f - fovNearness / 4;
                    }
                    fovNearness = Mathf.Clamp01(fovNearness);

					Vector2 screenPoint = Camera.main.WorldToScreenPoint(this.transform.position);
					screenPoint.x /= Screen.width;
					screenPoint.y /= Screen.height;
					screenPoint.x -= 0.5f;
					screenPoint.y -= 0.5f;
					float centerNearness = Mathf.Clamp01(1 - screenPoint.sqrMagnitude * 2);

					float scoreAverage = 0;

					if(directionSensitive)
					{
						Vector3 dir = this.transform.position - Camera.main.transform.position;
						Vector3 face = new Vector3(this.transform.rotation.x, this.transform.rotation.y, this.transform.rotation.z);
						dir.Normalize();
						face.Normalize();
						float faceNearness = ((Vector3.Dot(dir, face) + 1) / 2.0f);

						scoreAverage = (fovNearness + centerNearness + faceNearness) / 3;
					}
					else
					{
						scoreAverage = (fovNearness + centerNearness) / 2;
					}

					this.SendMessageUpwards("HitWithCamera", scoreAverage, SendMessageOptions.DontRequireReceiver);
					int points = (int) (bonusPoints * scoreAverage);
					Game.DataBank.photoScoreAdder += points;
					bonusPoints -= points;
					break;
				}
			}
		}
	}

}
